# Mapa Asambleas

Repo para trabajar en conjunto el mapa de las asambleas barriales y sitios de interés.

# Proyecto: El mapa de las asambleas


## Objetivo

La idea del proyecto es poner en pie una herramienta colectiva que permita cargar, centralizar y visualizar en un mapa, una red de información abierta y colaborativa de las asambleas que se realizan en todo el país, de manera que las mismas puedan ser administradas por responsables de cada una de ellas.


## Sobre el proyecto

La iniciativa, votada por la Asamblea de Scalabrini Ortiz y Corrientes, surge como propuesta de un intercambio entre integrantes de la Asambleas de Scalabrini Ortiz, de la Asamblea de Ángel Gallardo junto a integrantes del equipo de datos de La Izquierda Diario, como propuesta para desarrollar la difusión, extensión y organización de las asambleas. La participación en el desarrollo del proyecto, es abierta y colaborativa, a medios de comunicación, organizaciones, asambleístas y periodistas quieran impulsarlo. 


### Primera versión: 1.0

Se trabajará en una primera versión, lo más sencilla posible y que permita salir rápidamente. 
Consistirá en un sitio (tipo landing page) que en principio tendría dos páginas: 
1) Portada con mapas
2) Página para cargar la información de las asambleas (con una breve explicación sobre el proyecto)
Sobre la marcha se irán analizando las posibles mejoras y versiones más avanzadas que se puedan ir haciendo de acuerdo a las necesidades y posibilidades.


### Componentes y funcionamiento

La primera versión del proyecto en principio consistirá de:

Un dominio a registrar (sería mapadelasasambleas.com), para levantar un sitio (tipo landing page) que en principio tendría dos páginas: 1) portada con mapas y 2) Página para cargar información pública de las asambleas (con una breve explicación sobre el proyecto).
La carga de la información será abierta y chequeada antes de ser publicada.

Un formulario de Google, para cargar los datos de las asambleas, que permitirá cargar los siguientes datos:
Nombre (obligatorio), Dirección (obligatorio), Horario, Texto breve, Foto, Contactos, Redes (X,IG,FB,TikTok), Link a mapa zonal, Teléfono + email + observaciones de la o él responsable para chequear (qué no se mostrará públicamente).

Periódicamente se chequeará y geolocalizará la información cargada, después de lo cual se podrá visualizar:
En los mapas que estén en la página principal del sitio que serían 3 para mejor visualización: 
Todo el país, AMBA + La Plata y CABA en principio empezamos usando la plataforma flourish para visualizar
En cada punto de la asamblea se podrá visualizar la información pública cargada.
También se podrá consultar la hoja de cálculo que tendrá la misma información disponible públicamente y que podrá servir para cualquier visualización que se quiera hacer. Quizás podamos usar otra como Google Maps por ejemplo.


### Evolución del proyecto

Se realizarán periódicamente reuniones abiertas del proyecto con modalidad y fechas a definir, para intercambiar sobre mejoras y propuestas, tanto a nivel de funcionamiento como a nivel técnico (alternativas de desarrollos incluídas), con quienes quieran impulsarlo.
